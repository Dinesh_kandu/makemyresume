

<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <style type="Text/css">
            #i{
                       height:auto;
            margin:10px 12px;

float: left;
                
            }
#divhead
{
    margin-left:10px;margin-right:10px;
width:780px;
height:20px;
color:black;
font-weight:lighter;
background-color: #eaeaea;
position:relative;
border-radius:1px;

font-size: 14px;
}
            
        </style>
           <script type="text/javascript">
             //add column to table
function appendColumn() {
    var tbl = document.getElementById('myTable'), // table reference
        i;
    // open loop for each row and append cell
    for (i = 0; i < tbl.rows.length; i++) {
        createCell(tbl.rows[i].insertCell(tbl.rows[i].cells.length), i, 'col');
    }
}    
    function createCell(cell, text, style) {
    var div = document.createElement('div'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('class', style);        // set DIV class attribute
    div.setAttribute('className', style);    // set DIV class attribute for IE (?!)
    cell.appendChild(div);                   // append DIV to the table cell
}
    
    
             
               //hide table column
                function hideCol() {
            var col = document.getElementById("txtCol").value;
            if (isNaN(col) || col == "") {
                alert("Invalid Column");
                return;
            }
            col = parseInt(col, 10);
            col = col - 1;
            var tbl = document.getElementById("myTable");
            if (tbl != null) {
                if (col < 0 || col >= tbl.rows.length - 1) {
                    alert("Invalid Column");
                    return;
                }
                for (var i = 0; i < tbl.rows.length; i++) {
                    for (var j = 0; j < tbl.rows[i].cells.length; j++) {
                        tbl.rows[i].cells[j].style.display = "";
                        if (j == col)
                            tbl.rows[i].cells[j].style.display = "none";
                    }
                }
            }
        }
               
               
               
   //show hide hobbies div
   
   function showHideHobbies(ele) {
				var srcElement = document.getElementById(ele);
				if (srcElement != null) {
					if (srcElement.style.display == "block") {
						srcElement.style.display = 'none';
					}
					else {
						srcElement.style.display = 'block';
					}
					return false;
				}
			}
    
    
    
    
    
    
    
    
    //show hide skill div
    function showHideSkill(ele) {
				var srcElement = document.getElementById(ele);
				if (srcElement != null) {
					if (srcElement.style.display == "block") {
						srcElement.style.display = 'none';
					}
					else {
						srcElement.style.display = 'block';
					}
					return false;
				}
			}
    
   
    
    
    
    
    
    
    
    //show hide div table form
    
    function showHideDiv(ele) {
				var srcElement = document.getElementById(ele);
				if (srcElement != null) {
					if (srcElement.style.display == "block") {
						srcElement.style.display = 'none';
					}
					else {
						srcElement.style.display = 'block';
					}
					return false;
				}
			}
    
    
    
   
    
    
    
    
    
    //signature
                 
           
           function addsignFunction() {
  var x = document.getElementById("mysign").value;
  document.getElementById("sign").innerHTML = x;
}      
                 
                 
//declaration

           function adddclFunction() {
  var x = document.getElementById("mydcl").value;
  document.getElementById("dcl").innerHTML = x;
}
           
           //obj
           function addobjFunction() {
  var x = document.getElementById("myobj").value;
  document.getElementById("obj").innerHTML = x;
}
 

    //hobbies
    function addhobbiesFunction()
                 {
        var ul = document.getElementById("myhobbies");
    var candidate = document.getElementById("hobbies_id");
    var li = document.createElement("li");
    li.setAttribute('id',candidate.value);
    li.appendChild(document.createTextNode(candidate.value));
    ul.appendChild(li).style.textAlign = "left";    
        
        
           }

    function removehobbiesFunction() {
  
        /*var list = document.getElementById("myhobbies");
  list.removeChild(list.childNodes[0]);
  */
 
  var ul = document.getElementById("myhobbies");
    var candidate = document.getElementById("hobbies_id");
    var item = document.getElementById(candidate.value);
    ul.removeChild(item);
  
}


//softskill
  function addsoftFunction()
                 {
            var ul = document.getElementById("mysoftskill");
    var candidate = document.getElementById("soft_id");
    var li = document.createElement("li");
    li.setAttribute('id',candidate.value);
    li.appendChild(document.createTextNode(candidate.value));
    ul.appendChild(li).style.textAlign = "left";         
            
        
        /*
                     var node = document.createElement("LI");
  var textnode = document.createTextNode("software  skill");
  node.appendChild(textnode);
  document.getElementById("mysoftskill").appendChild(node);
                 
                 */
            }
                  function removesoftFunction() {
/*  var list = document.getElementById("mysoftskill");
  list.removeChild(list.childNodes[0]);
*/
var ul = document.getElementById("mysoftskill");
    var candidate = document.getElementById("soft_id");
    var item = document.getElementById(candidate.value);
    ul.removeChild(item);


}

//language
 function addlangFunction()
                 {
            var ul = document.getElementById("mylang");
    var candidate = document.getElementById("lang_id");
    var li = document.createElement("li");
    li.setAttribute('id',candidate.value);
    li.appendChild(document.createTextNode(candidate.value));
    ul.appendChild(li).style.textAlign = "left";         
        
        
        
        /*var node = document.createElement("LI");
  var textnode = document.createTextNode("Language");
  node.appendChild(textnode);
  document.getElementById("mylang").appendChild(node);
          */       }
                  function removelangFunction() {
  /*var list = document.getElementById("mylang");
  list.removeChild(list.childNodes[0]);

*/
var ul = document.getElementById("mylang");
    var candidate = document.getElementById("lang_id");
    var item = document.getElementById(candidate.value);
    ul.removeChild(item);


}

//skill
           function addItem(){
               try{
               var ul = document.getElementById("myList");
    var candidate = document.getElementById("skill_id");
    var li = document.createElement("li");
    li.setAttribute('id',candidate.value);
    li.appendChild(document.createTextNode(candidate.value));
    ul.appendChild(li).style.textAlign = "left";
               }
               catch(e)
               {
                   alert(e);
               }
               
              /* 
 var node = document.createElement("LI");
  var textnode = document.createTextNode("skill");
  node.appendChild(textnode);
  document.getElementById("myList").appendChild(node);   
        */
     
}

function removeItem(){
    try
    {
    var ul = document.getElementById("myList");
    var candidate = document.getElementById("skill_id");
    var item = document.getElementById(candidate.value);
    //ul.removeChild(item);
    ul.removeChild(item);
}
catch(e)
{
    alert(e);
}
    
//    ul.removeChild(myList.childNodes(item));
/*var list = document.getElementById("mylang");
  list.removeChild(list.childNodes[0]);
          */  
}
    
 
//Javascript code to Add new rows onclick of a button and to delete row .

var rowId = 0;
function addMoreRows() {

    var exam = document.getElementById('exam_id').value;
    var dici = document.getElementById('dicipline').value;
    var uni = document.getElementById('university').value;
    var yea = document.getElementById('yearofpassing').value;
    var marks = document.getElementById('marks').value;
        var table = document.getElementById('myTable');

    var row = table.insertRow();

 var rowBox = row.insertCell(0);
 rowBox.innerHTML = '<input type="checkbox" id="delete' + getRowId() + '">';
    var examName = row.insertCell(1);
    var diciName = row.insertCell(2);
    var  uniName= row.insertCell(3);
    var yeaName = row.insertCell(4);
    var marksName = row.insertCell(5);

 rowBox.innerHTML = '<input type="checkbox" id="delete' + getRowId() + '">';

    examName.innerHTML = exam;
    diciName.innerHTML = dici;
    uniName.innerHTML = uni;
 yeaName.innerHTML = yea;
    marksName.innerHTML = marks;

}


    function deleteMoreRows(tableID) {

    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    var selectedRows = getCheckedBoxes();

    selectedRows.forEach(function(currentValue) {
      deleteRowByCheckboxId(currentValue.id);
      
      
    });
}

function getRowId() {
  rowId += 1;
  return rowId;
}

function getRowIdsFromElements($array) {
  var arrIds = [];

  $array.forEach(function(currentValue, index, array){
    arrIds.push(getRowIdFromElement(currentValue));
  });

  return arrIds;
}

function getRowIdFromElement($el) {
    return $el.id.split('delete')[1];
}

//ref: http://stackoverflow.com/questions/8563240/how-to-get-all-checked-checkboxes
function getCheckedBoxes() {
  var inputs = document.getElementsByTagName("input");
  var checkboxesChecked = [];

  // loop over them all
  for (var i=0; i < inputs.length; i++) {
     // And stick the checked ones onto an array...
     if (inputs[i].checked) {
        checkboxesChecked.push(inputs[i]);
     }
  }

  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

//ref: http://stackoverflow.com/questions/4967223/delete-a-row-from-a-table-by-id
function deleteRowByCheckboxId(CheckboxId) {
    var checkbox = document.getElementById(CheckboxId);
    var row = checkbox.parentNode.parentNode;                //box, cell, row, table
    var table = row.parentNode;

    while ( table && table.tagName != 'TABLE' )
        table = table.parentNode;
    if (!table) return;
    table.deleteRow(row.rowIndex);
}
    
    
    
    
    
    
    
    
    
    
    
    
    //edit
    function myFunction() {
  document.getElementById("i").contentEditable = true;
  
}
   
   function saveFunction() {
  document.getElementById("i").contentEditable = false;

}
    
             
            function printDiv(i){
                var printContents =document.getElementById(i).innerHTML;
                var originalContents =document.body.innerHTML;
                document.body.innerHTML =printContents;
                document.body.style.backgroundColor = "#ffffff";
                window.print();
                 document.body.style.backgroundColor = "#008891";
               document.body.innerHTML =originalContents;
            }
        </script>
    </head>
    <body bgcolor="#008891" style="background-image:url('hands_frontpage.png') ;background-position: right top;background-repeat: no-repeat,repeat;  background-size: 1100px 1350px;">
         <div id="i" align="center" style="background-color:#ffffff;margin-top: 0px;margin-left: 0px" >
             
             
             
             
             
                <%@ page import="java.sql.*"%>
               <%@ page import="javax.sql.*"%>
                  
              <%
                  String user =String.valueOf(session.getAttribute("userid")); 
              String s11,s22,s33,s44,s55,s66,s77,s88,s99,s111,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,r27,r28,r29,r30,r31,r32,r33,r34,r35,r36,r37;
                 
      Class.forName("com.mysql.jdbc.Driver");
    
 Connection connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/resume","root","123456");
               Statement statement=connection.createStatement();
                String sql="SELECT * FROM biofresume";
                ResultSet resultset=null;
              resultset=statement.executeQuery(sql);
               String  d="I hereby affirm that the information in this document is accurate and true to the best of my knowledge.";
               while(resultset.next())
                 {
                     if(resultset.getString(1).equals(user))
                     {
   %>
           <center font-size="15px" > <h1><%
out.print(resultset.getString("fname")+" "+resultset.getString("mname")+" "+resultset.getString("lname"));
%></h1></center>
<div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891; ">
    <br>&nbsp;&nbsp;&nbsp;PERSONAL DETAILS</div>
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Father's Name "); %>&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("fathername"));
%>
</strong><br>
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Date Of Birth ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("dob"));
%>
</strong><br>
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Sex ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("gender"));
%>
</strong><br>    
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Religion ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("religion"));
%>
</strong><br>  
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Nationality ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("nationality"));
%></strong><br>

<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Email Id ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="color:skyblue"><%out.print(":"+resultset.getString("bfemail"));
%>
</b></strong><br>
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Mobile no ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("mobileno"));
%>
</strong>  <br>
<strong style="font-size:15px;float:left;color:black;margin-left:10px;margin-right:10px "><%
out.print("Address ");%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(":"+resultset.getString("street")+",near-"+resultset.getString("landmark")+","+resultset.getString("city")+","+resultset.getString("state")+","+resultset.getString("country")+",Pin no :"+resultset.getString("pincode"));
%></strong>
<br>
<div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;"><br>
     &nbsp;&nbsp;&nbsp;OBJECTIVES</div>
<p align="left" id="obj">
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%
out.print(resultset.getString("objectives"));
 %></p>
 <br>
 <div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;">
     <br>&nbsp;&nbsp;&nbsp;EDUCATION DETAILS</div>
 <br>
   <table border="1" id="myTable" style="margin-left:10px;margin-right:10px">
    <tr>
        <th><b>Checkit</b></th>
        <th><b>Examination</b></th>
        <th><b>Dicipline</b></th>
        <th><b>University/School</b></th>
        <th><b>year of passing</b></th>
        <th><b>%</b></th>
        
    </tr>
   
      <tr>
          <td><input type='checkbox' id='delete' /></td>
        <td><%
out.print(resultset.getString("ssccourse"));
 %></td>
        <td><%
out.print(resultset.getString("sscdecipline"));
 %></td>
        <td><%
out.print(resultset.getString("sscunivesity"));
 %></td>
        <td><%
out.print(resultset.getString("sscgraduyear"));
 %></td>
        <td><%
out.print(resultset.getString("sscpercentage"));
 %></td>
        
    </tr>
    
    
    
    
    <tr>
              <td><input type='checkbox' id='delete'/></td>
       <td><%
out.print(resultset.getString("hsccourse"));
 %></td>
        <td><%
out.print(resultset.getString("hscdecipline"));
 %></td>
        <td><%
out.print(resultset.getString("hscunivesity"));
 %></td>
        <td><%
out.print(resultset.getString("hscgraduyear"));
 %></td>
        <td><%
out.print(resultset.getString("hscpercentage"));
 %></td>
        
        
    </tr>
   
    <tr>
        <td><input type='checkbox' id='delete'/></td>
        <td><%
out.print(resultset.getString("bqcourse"));
 %></td>
        <td><%
out.print(resultset.getString("bqdecipline"));
 %></td>
        <td><%
out.print(resultset.getString("bqunivesity"));
 %></td>
        <td><%
out.print(resultset.getString("bqgraduyear"));
 %></td>
        <td><%
out.print(resultset.getString("bqpercentage"));
 %></td>
        
        
    </tr>
    
    <% 
if(resultset.getString(25).length()>0)
{
    %>
    
     <tr>
<td><input type='checkbox' id='delete'/></td>
         <td><%
out.print(resultset.getString("pgcourse"));
 %></td>
        <td><%
out.print(resultset.getString("pgdecipline"));
 %></td>
        <td><%
out.print(resultset.getString("pgunivesity"));
 %></td>
        <td><%
out.print(resultset.getString("pggraduyear"));
 %></td>
        <td><%
out.print(resultset.getString("pgpercentage"));
 %></td>
        
    </tr>
        <%}
%>
 </table>
  <br>

 <br>
 <%
if(resultset.getString("pskills1") !=null)
{
%>
 <div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;">
     <br>&nbsp;&nbsp;&nbsp;SKILLS</div>
   
    <ul id="myList" style="margin-left:10px;margin-right:10px">
 
                        <li align="left">  <%
out.print(resultset.getString("skills1"));
%></li>
                        <li align="left">     <%
out.print(resultset.getString("skills2"));
%></li>
                        
                        <li align="left">     <%
out.print(resultset.getString("skills3"));
%></li>
                                         
                        <li align="left">     <%
out.print(resultset.getString("skills4"));
%></li>
    </ul>
   
    <br>
    <%
}
if(resultset.getString("pskills1").length()>0)
{
    %>
  
<div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;">
    <br>&nbsp;&nbsp;&nbsp;SOFTWARE SKILLS</div>

    <ul id="mysoftskill" style="margin-left:10px;margin-right:10px">
 
                        <li align="left">    <% out.print("Lnguages "); %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <% out.print(":"+resultset.getString("pskills1")+","+resultset.getString("pskills2")+","+resultset.getString("pskills3")+","+resultset.getString("pskills4")); %></li>
                        
                        <li align="left">     <% out.print("Operating System "); %>&nbsp;&nbsp;&nbsp;&nbsp; <% out.print(":"+resultset.getString("os1")+","+resultset.getString("os2")+","+resultset.getString("os3")); %></li>
                        
                        <li align="left">      <% out.print("Databases "); %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <% out.print(":"+resultset.getString("db1")+","+resultset.getString("db2")+","+resultset.getString("db3")); %></li>
                                          
                        <li align="left">     <% out.print("Markup & script "); %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <% out.print(":"+resultset.getString("markupscript")+","+resultset.getString("markupscript2")); %></li>
                          <li align="left">      <% out.print("Web Application "); %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <% out.print(":"+resultset.getString("web1")+","+resultset.getString("web2")); %></li>
    </ul>
   
    <br>
    <%
}
%>
    <div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;">
        <br>&nbsp;&nbsp;&nbsp;HOBBIES</div>
    <ul id="myhobbies" style="margin-left:10px;margin-right:10px">
  
                        <li align="left">     <%
out.print(resultset.getString("hobbies1"));
%></li>
                        <li align="left">
                        <%
out.print(resultset.getString("hobbies2"));
%></li>
                       
                        <li align="left">     <%
out.print(resultset.getString("hobbies3"));
%></li>
        </ul>
    <br>
    <div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;">
        <br>&nbsp;&nbsp;&nbsp;LANGUAGE KNOW</div>
<ul id="mylang" style="margin-left:10px;margin-right:10px">
    <li  align="left">
        <%
out.print(resultset.getString("firstlanguage"));
%>
    </li>
    <li align="left">
        <%
out.print(resultset.getString("secondlanguage"));
%>
    </li>
    <li align="left">
        <%
out.print(resultset.getString("thirdlanguage"));
%>
    </li>
    </ul>
    <br>
    <div id="divhead" align="left" style=" border-radius: 15px 50px 30px;height:35px;background:#008891;">
        <br>&nbsp;&nbsp;&nbsp;DECLARATION</div>
<p align="left" id="dcl" style="margin-left:10px;margin-right:10px">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%
out.print(d);
 %></p>
<br>
<h1 align="right" style="margin-left:10px;margin-right:10px">SIGNATURE</h1>

<h3 align="right" id="sign" style="margin-left:10px;margin-right:10px"><%
out.print(resultset.getString("fname")+" "+resultset.getString("mname")+" "+resultset.getString("lname"));
 %> </h3>
 <br>
 <%   
                     }
                     }
                
              
                 %>
                  </div>
                  
  <input type="button" onclick="printDiv('i')" value="Print" />
   <input type="button" value=" Edit" onclick="myFunction()" name="button">
   <input type="button" value=" Save" onclick="saveFunction()" ><br><br>
   <input type="button" value="Add/Delete Qualification" onClick="showHideDiv('table')"/><br><br>
<input type="button" value="Add/Delete Skill" onClick="showHideSkill('skilldiv')"/><br><br>
<input type="button" value="Add/Delete Hobbies" onClick="showHideSkill('hobbiesdiv')"/><br><br>
<input type="button" value="Add/Delete Language" onClick="showHideSkill('langdiv')"/><br><br>
<input type="button" value="Add/Delete Software Skill" onClick="showHideSkill('softdiv')"/><br><br>
<input type="button" value="Add/Delete Objective" onClick="showHideSkill('objdiv')"/><br><br>
<input type="button" value="Add/Delete Declaration" onClick="showHideSkill('dcldiv')"/><br><br>
<input type="button" value="Add/Delete Signature" onClick="showHideSkill('signdiv')"/>
<br><br><br>
   <div id="table"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:850px  ">
   <form>
       Enter your examination : <input type="text" name="users" id="exam_id"  onfocus="if(this.value == 'name') {this.value=''}" onblur="if(this.value == ''){this.value ='name'}"><br><br>
   
   Enter your dicipline:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="dicipline" name="dicipline"><br><br>
   Enter your university: &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="university" name="university"><br><br>
   Enter your year :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="yearofpassing" name="year of passing"><br><br>
   Enter your Marks: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="marks" name="marks"><br><br>
     <input type="button" id="mysubmit" value="Add Row" onClick="addMoreRows()">
   <input type="button" id="delete" value="Delete Row" onClick="deleteMoreRows('myTable')">
   <input type="reset" value="reset"><br><br>
   Enter Column Number :
        <input type="text" id="txtCol" /><input type="button" value="Hide" onclick="hideCol();" />
        <input type="button" value="Add a Column" onclick="appendColumn()">

        
        <br />
       
   </form>
   </div><br><br><br>
           <div id="skilldiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px ">
   <form>
 Value : <input type="text" name="skill" id="skill_id">
 <br><br>
 
  <input type="button" value="AddSkill" onclick="addItem()" >
     
     <input type="button" value="Removeskill" onclick="removeItem()">
   </form></div><br><br>
     <div id="hobbiesdiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px">
    <form>
 Value : <input type="text" name="hobbies" id="hobbies_id">
 <br>
 <br>
        <input type="button" value="AddHobbies" onclick="addhobbiesFunction()" >
     
     <input type="button" value="Removehobbies" onclick="removehobbiesFunction()">
    </form>
         </div><br><br>
         <div id="langdiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px">
    <form>
        Value : <input type="text" name="lang" id="lang_id">
 <br>
 <br>
      <input type="button" value="AddLanguage" onclick="addlangFunction()" >
     <input type="button" value="Removelanguage" onclick="removelangFunction()">
    </form></div>
    <br><br>
    <div id="softdiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px">
    <form>
        Value : <input type="text" name="soft" id="soft_id">
 <br>
 <br>
       <input type="button" value="AddSkill" onclick="addsoftFunction()" >
     <input type="button" value="Removeskill" onclick="removesoftFunction()">
    </form>
    </div><br><br>
       <div id="objdiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px"> <form>
     
        Value : <input type="text"  id="myobj"><br><br>
        
     <input type="button" value="ChangeObjective" onclick="addobjFunction()">   
    </form>
   </div>
    <br><br>
    <div id="dcldiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px"><form>
     
        Value : <input type="text"  id="mydcl"><br><br>
        
     <input type="button" value="ChangeDeclaration" onclick="adddclFunction()">   
    </form>
    </div>
    <br><br>
    <div id="signdiv"  style="display:none;background-color:#ffffff;padding-top:5px;padding-bottom:5px;margin-left:50px;margin-right:950px;"><form>
     
        Value : <input type="text"  id="mysign"><br><br>
        
     <input type="button" value="ChangeSignature" onclick="addsignFunction()">   
        </form></div>
    </body>
</html>